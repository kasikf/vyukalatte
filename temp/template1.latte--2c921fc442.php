<?php

use Latte\Runtime as LR;

/** source: template/template1.latte */
final class Template2c921fc442 extends Latte\Runtime\Template
{
	public const Source = 'template/template1.latte';

	public const Blocks = [
		['title' => 'blockTitle'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ukázka template</title>
</head>
<body>
';
		$this->renderBlock('title', get_defined_vars(), 'html') /* line 9 */;
		echo '
    <h2>Výpis PHP proměnné </h2>
    <p>Proměná ovoce: ';
		echo LR\Filters::escapeHtmlText($ovoce) /* line 12 */;
		echo '</p>

    <h2>Výpis pole hodnot</h2>
    <ol>
';
		foreach ($items1 as $item) /* line 16 */ {
			echo '        <li>';
			echo LR\Filters::escapeHtmlText($item) /* line 17 */;
			echo '</li>
';

		}

		echo '    </ol>

    <h2>Výpis pole hodnot pomocí n:atribut;</h2>
    <ol>
';
		foreach ($objednavka as $item) /* line 23 */ {
			echo '        <li>';
			echo LR\Filters::escapeHtmlText($item) /* line 23 */;
			echo '</li>
';

		}

		echo '    </ol>

    <h2>Použití podmínek - výpis hodnoty "two" barevně</h2>
   <ol>
';
		foreach ($items1 as $item) /* line 28 */ {
			echo '        <li>
';
			if ($item == 'two') /* line 29 */ {
				echo '            <div style="color:green;">';
				echo LR\Filters::escapeHtmlText($item) /* line 30 */;
				echo '</div>
';
			} else /* line 31 */ {
				echo '            ';
				echo LR\Filters::escapeHtmlText($item) /* line 32 */;
				echo "\n";
			}
			echo '        </li>
';

		}

		echo '    </ol>

    <h2>Podmínky</h2>
    <p>Úkol: Vypište dle hodnoty proměnné $ovoce alkoholický nápoj, který se z něj vyrábí (jablko,švestka,hruška)</p>
';
		if ($ovoce == 'jablko') /* line 39 */ {
			echo '        <p>Jabkovice</p>
';
		} elseif ($ovoce == 'švestka') /* line 41 */ {
			echo '        <p>Slivovice</p>
';
		} elseif ($ovoce == 'hruška') /* line 43 */ {
			echo '        <p>Hruškovice</p>
';
		}


		echo '
    <h2>Ternární operátor</h2>
    <p>Úkol: Vypište dle hodnoty proměnné $rezervace větu Rezervováno nebo Volné k pronájmu</p>
    <p>';
		echo LR\Filters::escapeHtmlText($rezervace ? 'Rezervováno' : 'Volné k pronájmu') /* line 49 */;
		echo '</p>

    <h2>Cyklus Foreach</h2>
    <p>Úkol: Vypište pole $objednávka jako HTML seznam</p>
    <ol>
';
		foreach ($objednavka as $item) /* line 54 */ {
			echo '        <li>';
			echo LR\Filters::escapeHtmlText($item) /* line 54 */;
			echo '</li>
';

		}

		echo '    </ol>

    <h2>Cyklus For</h2>
    <p>Úkol: Vypište první tři položky pole $objednávka jako HTML seznam (formát výpisu - 1. položka 2.položka atd.)</p>
    <ol>
';
		for ($i = 0;
		$i < 3;
		$i++) /* line 60 */ {
			echo '        <li>';
			echo LR\Filters::escapeHtmlText($objednavka[$i]) /* line 61 */;
			echo '</li>
';

		}
		echo '    </ol>
    
    <h2>Bloky</h2>
    <p>Úkol: Vytvořte blok HTML kódu a použijte na jiném místě šablony</p>
';
		$this->renderBlock('title', get_defined_vars()) /* line 67 */;
		echo '
    <h2>Tabulka</h2>
    <table>
';
		foreach ($iterator = $ʟ_it = new Latte\Essential\CachingIterator($people, $ʟ_it ?? null) as $index => $person) /* line 74 */ {
			echo '        <tr';
			echo ($ʟ_tmp = array_filter([$iterator->odd ? 'odd' : null])) ? ' class="' . LR\Filters::escapeHtmlAttr(implode(" ", array_unique($ʟ_tmp))) . '"' : "" /* line 74 */;
			echo '>
';
			foreach ($person as $name) /* line 75 */ {
				echo '            <td>';
				echo LR\Filters::escapeHtmlText($name) /* line 75 */;
				echo '</td>
';

			}

			echo '        </tr>
';

		}
		$iterator = $ʟ_it = $ʟ_it->getParent();

		echo '    </table>
</body>
</html>';
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['item' => '16, 23, 28, 54', 'index' => '74', 'person' => '74', 'name' => '75'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		return get_defined_vars();
	}


	/** {block title} on line 67 */
	public function blockTitle(array $ʟ_args): void
	{
		echo '        <h1 style="font-size: 2rem;">Cvičení</h1>
        <p>lorem ipsum ...</p>
';
	}
}
