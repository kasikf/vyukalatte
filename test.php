<?php 
    require_once("vendor/autoload.php");
    $latte = new Latte\Engine;

    $latte->setTempDirectory('temp');
    
    $params = [
        'items1' => ['one', 'two', 'three'],
        'items2' => 'apple',
        'ovoce' => 'hruška',
        'rezervace' => false,
        'objednavka' => ['sluchátka','procesor','monitor','case','kamera','klavesnice'],
        'people' => array(array("Jan","Novák"),array("Karel","Novák"),array("Pavel","Novák"))
    ];
    
    // kresli na výstup
    $latte->render('template/template1.latte', $params);
?>